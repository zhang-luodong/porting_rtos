################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/rt-thread/src/clock.c \
../src/rt-thread/src/components.c \
../src/rt-thread/src/cpu.c \
../src/rt-thread/src/device.c \
../src/rt-thread/src/idle.c \
../src/rt-thread/src/ipc.c \
../src/rt-thread/src/irq.c \
../src/rt-thread/src/kservice.c \
../src/rt-thread/src/mem.c \
../src/rt-thread/src/memheap.c \
../src/rt-thread/src/mempool.c \
../src/rt-thread/src/object.c \
../src/rt-thread/src/scheduler.c \
../src/rt-thread/src/signal.c \
../src/rt-thread/src/slab.c \
../src/rt-thread/src/thread.c \
../src/rt-thread/src/timer.c 

OBJS += \
./src/rt-thread/src/clock.o \
./src/rt-thread/src/components.o \
./src/rt-thread/src/cpu.o \
./src/rt-thread/src/device.o \
./src/rt-thread/src/idle.o \
./src/rt-thread/src/ipc.o \
./src/rt-thread/src/irq.o \
./src/rt-thread/src/kservice.o \
./src/rt-thread/src/mem.o \
./src/rt-thread/src/memheap.o \
./src/rt-thread/src/mempool.o \
./src/rt-thread/src/object.o \
./src/rt-thread/src/scheduler.o \
./src/rt-thread/src/signal.o \
./src/rt-thread/src/slab.o \
./src/rt-thread/src/thread.o \
./src/rt-thread/src/timer.o 

C_DEPS += \
./src/rt-thread/src/clock.d \
./src/rt-thread/src/components.d \
./src/rt-thread/src/cpu.d \
./src/rt-thread/src/device.d \
./src/rt-thread/src/idle.d \
./src/rt-thread/src/ipc.d \
./src/rt-thread/src/irq.d \
./src/rt-thread/src/kservice.d \
./src/rt-thread/src/mem.d \
./src/rt-thread/src/memheap.d \
./src/rt-thread/src/mempool.d \
./src/rt-thread/src/object.d \
./src/rt-thread/src/scheduler.d \
./src/rt-thread/src/signal.d \
./src/rt-thread/src/slab.d \
./src/rt-thread/src/thread.d \
./src/rt-thread/src/timer.d 


# Each subdirectory must supply rules for building sources it contributes
src/rt-thread/src/%.o: ../src/rt-thread/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v8 gcc compiler'
	aarch64-none-elf-gcc -Wall -O0 -g3 -ID:/LearningMaterials/NAS/ZYNQ/AXU2CGB/StudyDoc/course_s2/00_zld/ps_rtt_hello/vitis/design_1_wrapper/export/design_1_wrapper/sw/design_1_wrapper/domain_psu_cortexa53_0/bspinclude/include -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\driver_vitis" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\cortex-a53" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\finsh" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\drivers\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\applications" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\libc\time" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common\gic" -c -fmessage-length=0 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


