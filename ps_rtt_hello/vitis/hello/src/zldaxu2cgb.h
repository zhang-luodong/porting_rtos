//based on raspberryPI4' head file make
#ifndef __AXU2CGB_H__
#define __AXU2CGB_H__

//gpio
#define GPIO_BASE (0xFE000000 + 0x00200000)

//uart
#define UART1_BASE                  (0xFF010000)
#define PL011_BASE                  UART1_BASE
#define IRQ_PL011                   (54)
#define UART_REFERENCE_CLOCK        (99999001)

// 0x40, 0x44, 0x48, 0x4c: Core 0~3 Timers interrupt control
#define CORE0_TIMER_IRQ_CTRL    HWREG32(0xFF800040)
#define TIMER_IRQ               30
#define NON_SECURE_TIMER_IRQ    (1 << 1)

//gic max
#define INTC_BASE                   (0xF9020000U)
#define GIC_V2_DISTRIBUTOR_BASE     (0xF9010000U)
#define GIC_V2_CPU_INTERFACE_BASE   (INTC_BASE + 0x00042000)
#define GIC_V2_HYPERVISOR_BASE      (INTC_BASE + 0x00044000)
#define GIC_V2_VIRTUAL_CPU_BASE     (INTC_BASE + 0x00046000)

#define GIC_PL400_DISTRIBUTOR_PPTR  0xF9010000
#define GIC_PL400_CONTROLLER_PPTR   0xF9020000



rt_inline rt_uint32_t platform_get_gic_dist_base(void)
{
    return GIC_PL400_DISTRIBUTOR_PPTR;
}

rt_inline rt_uint32_t platform_get_gic_cpu_base(void)
{
    return GIC_PL400_CONTROLLER_PPTR;
}

#endif
