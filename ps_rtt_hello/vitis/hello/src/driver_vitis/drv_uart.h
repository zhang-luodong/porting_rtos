/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2020-04-16     bigmagic       first version
 */

#ifndef DRV_UART_H__
#define DRV_UART_H__



int rt_hw_uart_init(void);

#endif /* DRV_UART_H__ */
