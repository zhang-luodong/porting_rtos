connect -url tcp:127.0.0.1:3121
source D:/WorkApplication/Xilinx_Vitis/Vitis/2020.1/scripts/vitis/util/zynqmp_utils.tcl
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw D:/LearningMaterials/NAS/ZYNQ/AXU2CGB/StudyDoc/course_s2/00_zld/ps_rtt_hello/vitis/design_1_wrapper/export/design_1_wrapper/hw/design_1_wrapper.xsa -mem-ranges [list {0x80000000 0xbfffffff} {0x400000000 0x5ffffffff} {0x1000000000 0x7fffffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
set mode [expr [mrd -value 0xFF5E0200] & 0xf]
mask_write 0xFF5E0200 0xf000 0
targets -set -nocase -filter {name =~ "*A53*#0"}
rst -processor
dow D:/LearningMaterials/NAS/ZYNQ/AXU2CGB/StudyDoc/course_s2/00_zld/ps_rtt_hello/vitis/design_1_wrapper/export/design_1_wrapper/sw/design_1_wrapper/boot/fsbl.elf
set bp_25_50_fsbl_bp [bpadd -addr &XFsbl_Exit]
con -block -timeout 60
bpremove $bp_25_50_fsbl_bp
targets -set -nocase -filter {name =~ "*A53*#0"}
rst -processor
dow D:/LearningMaterials/NAS/ZYNQ/AXU2CGB/StudyDoc/course_s2/00_zld/ps_rtt_hello/vitis/hello/Debug/hello.elf
configparams force-mem-access 0
bpadd -addr &main
