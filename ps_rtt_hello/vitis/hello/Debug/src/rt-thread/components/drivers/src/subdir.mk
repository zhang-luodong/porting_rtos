################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/rt-thread/components/drivers/src/completion.c \
../src/rt-thread/components/drivers/src/dataqueue.c \
../src/rt-thread/components/drivers/src/pipe.c \
../src/rt-thread/components/drivers/src/ringblk_buf.c \
../src/rt-thread/components/drivers/src/ringbuffer.c \
../src/rt-thread/components/drivers/src/waitqueue.c \
../src/rt-thread/components/drivers/src/workqueue.c 

OBJS += \
./src/rt-thread/components/drivers/src/completion.o \
./src/rt-thread/components/drivers/src/dataqueue.o \
./src/rt-thread/components/drivers/src/pipe.o \
./src/rt-thread/components/drivers/src/ringblk_buf.o \
./src/rt-thread/components/drivers/src/ringbuffer.o \
./src/rt-thread/components/drivers/src/waitqueue.o \
./src/rt-thread/components/drivers/src/workqueue.o 

C_DEPS += \
./src/rt-thread/components/drivers/src/completion.d \
./src/rt-thread/components/drivers/src/dataqueue.d \
./src/rt-thread/components/drivers/src/pipe.d \
./src/rt-thread/components/drivers/src/ringblk_buf.d \
./src/rt-thread/components/drivers/src/ringbuffer.d \
./src/rt-thread/components/drivers/src/waitqueue.d \
./src/rt-thread/components/drivers/src/workqueue.d 


# Each subdirectory must supply rules for building sources it contributes
src/rt-thread/components/drivers/src/%.o: ../src/rt-thread/components/drivers/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v8 gcc compiler'
	aarch64-none-elf-gcc -Wall -O0 -g3 -ID:/LearningMaterials/NAS/ZYNQ/AXU2CGB/StudyDoc/course_s2/00_zld/ps_rtt_hello/vitis/design_1_wrapper/export/design_1_wrapper/sw/design_1_wrapper/domain_psu_cortexa53_0/bspinclude/include -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\driver_vitis" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\cortex-a53" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\finsh" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\drivers\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\applications" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\libc\time" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common\gic" -c -fmessage-length=0 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


