/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2020-04-16     bigmagic       first version
 */

#include <rthw.h>
#include <rtthread.h>
#include <rtdevice.h>

#include "xparameters.h" //导入串口设备号需要这个头文件
#include <xuartps_hw.h>

#include "board.h"
#include "drv_uart.h"
#include "drv_gpio.h"

#include "zldaxu2cgb.h"

#define UART1_BASE (0xFF010000) //uart1寄存器地址
#define IRQ_UART1  (54)   //uart1中断号

#define UART_BUFFER_SIZE            255
#define UART_DEVICE_ID              XPAR_XUARTPS_0_DEVICE_ID
#define XUARTPS_TOTAL_BYTES         (u8)32

XUartPs Uart_PS;
static u8 SendBuffer[UART_BUFFER_SIZE];	/* Buffer for Transmitting Data */
static u8 RecvBuffer[UART_BUFFER_SIZE];	/* Buffer for Receiving Data */
static u8 ReturnString[XUARTPS_TOTAL_BYTES];

struct hw_uart_device
{
    rt_ubase_t hw_base;
    rt_uint32_t irqno;
};

static rt_err_t uart_configure(struct rt_serial_device *serial, struct serial_configure *cfg)
{
    struct hw_uart_device *uart;
    rt_uint32_t IntrMask = XUARTPS_IXR_RXOVR;
	int Status;
	XUartPs_Config *Config;
	u16 Index;

	RT_ASSERT(serial != RT_NULL);
	uart = (struct hw_uart_device *)serial->parent.user_data;

	Config = XUartPs_LookupConfig(UART_DEVICE_ID);
	if (NULL == Config) {
		return XST_FAILURE;
	}
	Status = XUartPs_CfgInitialize(&Uart_PS, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	XUartPs_SetBaudRate(&Uart_PS, 115200);

	Status = XUartPs_SelfTest(&Uart_PS);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	/* Use normal mode. */
	XUartPs_SetOperMode(&Uart_PS, XUARTPS_OPER_MODE_NORMAL);

	XUartPs_SetFifoThreshold(&Uart_PS, 1);
	XUartPs_SetInterruptMask(&Uart_PS, IntrMask);
	rt_hw_interrupt_umask(uart->irqno);

	return RT_EOK;
}

static rt_err_t uart_control(struct rt_serial_device *serial, int cmd, void *arg)
{
    struct hw_uart_device *uart;

    RT_ASSERT(serial != RT_NULL);
    uart = (struct hw_uart_device *)serial->parent.user_data;

    switch (cmd)
    {
    case RT_DEVICE_CTRL_CLR_INT:
        /* disable rx irq */
        PL011_REG_IMSC(uart->hw_base) &= ~((uint32_t)PL011_IMSC_RXIM);
        rt_hw_interrupt_mask(uart->irqno);
        break;

    case RT_DEVICE_CTRL_SET_INT:
        /* enable rx irq */
        PL011_REG_IMSC(uart->hw_base) |= PL011_IMSC_RXIM;
        rt_hw_interrupt_umask(uart->irqno);
        break;
    }

    return RT_EOK;
}

static int uart_putc(struct rt_serial_device *serial, char c)
{
    struct hw_uart_device *uart;

    RT_ASSERT(serial != RT_NULL);
    uart = (struct hw_uart_device *)serial->parent.user_data;

    while ((PL011_REG_FR(uart->hw_base) & PL011_FR_TXFF));
    PL011_REG_DR(uart->hw_base) = (uint8_t)c;

    return 1;
}

static int uart_getc(struct rt_serial_device *serial)
{
    int ch = -1;
    struct hw_uart_device *uart;

    RT_ASSERT(serial != RT_NULL);
    uart = (struct hw_uart_device *)serial->parent.user_data;

    if((PL011_REG_FR(uart->hw_base) & PL011_FR_RXFE) == 0)
    {
        ch = PL011_REG_DR(uart->hw_base) & 0xff;
    }

    return ch;
}

static const struct rt_uart_ops _uart_ops =
{
    uart_configure,
    uart_control,
    uart_putc,
    uart_getc,
};


static void rt_hw_uart_isr(int irqno, void *param)
{
    struct rt_serial_device *serial = (struct rt_serial_device*)param;
    struct hw_uart_device *uart_device = (struct uart_device *)serial->parent.user_data;
    rt_uint32_t uart_sr;

    uart_sr = XUartPs_ReadReg(uart_device->hw_base, XUARTPS_IMR_OFFSET);
    uart_sr &= XUartPs_ReadReg(uart_device->hw_base, XUARTPS_ISR_OFFSET);

    if(uart_sr & XUARTPS_IXR_RXOVR)
    {
    	rt_hw_serial_isr(serial, RT_SERIAL_EVENT_RX_IND);
    }

    XUartPs_WriteReg(uart_device->hw_base , XUARTPS_ISR_OFFSET, uart_sr);
}

/* UART device driver structure */
static struct hw_uart_device _uart1_device =
{
    UART1_BASE,
    IRQ_UART1,
};

static struct rt_serial_device _serial1;

int rt_hw_uart_init(void)
{
    struct hw_uart_device *uart;
    struct serial_configure config = RT_SERIAL_CONFIG_DEFAULT;

    uart = &_uart1_device;

    _serial1.ops    = &_uart_ops;
    _serial1.config = config;

    /* register UART1 device */
    rt_hw_serial_register(&_serial1, "uart1",
                          RT_DEVICE_FLAG_RDWR | RT_DEVICE_FLAG_INT_RX,
                          uart);
    rt_hw_interrupt_install(uart->irqno, rt_hw_uart_isr, &_serial1, "uart1");

    return 0;
}
