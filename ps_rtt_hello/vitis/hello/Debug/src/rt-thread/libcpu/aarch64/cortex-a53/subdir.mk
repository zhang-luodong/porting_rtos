################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/rt-thread/libcpu/aarch64/cortex-a53/interrupt.c \
../src/rt-thread/libcpu/aarch64/cortex-a53/stack.c \
../src/rt-thread/libcpu/aarch64/cortex-a53/trap.c 

S_UPPER_SRCS += \
../src/rt-thread/libcpu/aarch64/cortex-a53/entry_point.S 

OBJS += \
./src/rt-thread/libcpu/aarch64/cortex-a53/entry_point.o \
./src/rt-thread/libcpu/aarch64/cortex-a53/interrupt.o \
./src/rt-thread/libcpu/aarch64/cortex-a53/stack.o \
./src/rt-thread/libcpu/aarch64/cortex-a53/trap.o 

S_UPPER_DEPS += \
./src/rt-thread/libcpu/aarch64/cortex-a53/entry_point.d 

C_DEPS += \
./src/rt-thread/libcpu/aarch64/cortex-a53/interrupt.d \
./src/rt-thread/libcpu/aarch64/cortex-a53/stack.d \
./src/rt-thread/libcpu/aarch64/cortex-a53/trap.d 


# Each subdirectory must supply rules for building sources it contributes
src/rt-thread/libcpu/aarch64/cortex-a53/%.o: ../src/rt-thread/libcpu/aarch64/cortex-a53/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v8 gcc compiler'
	aarch64-none-elf-gcc -Wall -O0 -g3 -ID:/LearningMaterials/NAS/ZYNQ/AXU2CGB/StudyDoc/course_s2/00_zld/ps_rtt_hello/vitis/design_1_wrapper/export/design_1_wrapper/sw/design_1_wrapper/domain_psu_cortexa53_0/bspinclude/include -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\driver_vitis" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\cortex-a53" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\finsh" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\drivers\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\applications" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\libc\time" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common\gic" -c -fmessage-length=0 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/rt-thread/libcpu/aarch64/cortex-a53/%.o: ../src/rt-thread/libcpu/aarch64/cortex-a53/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v8 gcc compiler'
	aarch64-none-elf-gcc -Wall -O0 -g3 -ID:/LearningMaterials/NAS/ZYNQ/AXU2CGB/StudyDoc/course_s2/00_zld/ps_rtt_hello/vitis/design_1_wrapper/export/design_1_wrapper/sw/design_1_wrapper/domain_psu_cortexa53_0/bspinclude/include -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\driver_vitis" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\cortex-a53" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\finsh" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\drivers\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\applications" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\libc\time" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common\gic" -c -fmessage-length=0 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


