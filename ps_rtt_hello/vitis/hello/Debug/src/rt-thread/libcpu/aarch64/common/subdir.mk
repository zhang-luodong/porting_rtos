################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/rt-thread/libcpu/aarch64/common/cpu.c \
../src/rt-thread/libcpu/aarch64/common/mmu.c 

S_UPPER_SRCS += \
../src/rt-thread/libcpu/aarch64/common/cache.S \
../src/rt-thread/libcpu/aarch64/common/context_gcc.S \
../src/rt-thread/libcpu/aarch64/common/cpu_gcc.S \
../src/rt-thread/libcpu/aarch64/common/startup_gcc.S \
../src/rt-thread/libcpu/aarch64/common/vector_gcc.S 

OBJS += \
./src/rt-thread/libcpu/aarch64/common/cache.o \
./src/rt-thread/libcpu/aarch64/common/context_gcc.o \
./src/rt-thread/libcpu/aarch64/common/cpu.o \
./src/rt-thread/libcpu/aarch64/common/cpu_gcc.o \
./src/rt-thread/libcpu/aarch64/common/mmu.o \
./src/rt-thread/libcpu/aarch64/common/startup_gcc.o \
./src/rt-thread/libcpu/aarch64/common/vector_gcc.o 

S_UPPER_DEPS += \
./src/rt-thread/libcpu/aarch64/common/cache.d \
./src/rt-thread/libcpu/aarch64/common/context_gcc.d \
./src/rt-thread/libcpu/aarch64/common/cpu_gcc.d \
./src/rt-thread/libcpu/aarch64/common/startup_gcc.d \
./src/rt-thread/libcpu/aarch64/common/vector_gcc.d 

C_DEPS += \
./src/rt-thread/libcpu/aarch64/common/cpu.d \
./src/rt-thread/libcpu/aarch64/common/mmu.d 


# Each subdirectory must supply rules for building sources it contributes
src/rt-thread/libcpu/aarch64/common/%.o: ../src/rt-thread/libcpu/aarch64/common/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v8 gcc compiler'
	aarch64-none-elf-gcc -Wall -O0 -g3 -ID:/LearningMaterials/NAS/ZYNQ/AXU2CGB/StudyDoc/course_s2/00_zld/ps_rtt_hello/vitis/design_1_wrapper/export/design_1_wrapper/sw/design_1_wrapper/domain_psu_cortexa53_0/bspinclude/include -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\driver_vitis" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\cortex-a53" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\finsh" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\drivers\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\applications" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\libc\time" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common\gic" -c -fmessage-length=0 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/rt-thread/libcpu/aarch64/common/%.o: ../src/rt-thread/libcpu/aarch64/common/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v8 gcc compiler'
	aarch64-none-elf-gcc -Wall -O0 -g3 -ID:/LearningMaterials/NAS/ZYNQ/AXU2CGB/StudyDoc/course_s2/00_zld/ps_rtt_hello/vitis/design_1_wrapper/export/design_1_wrapper/sw/design_1_wrapper/domain_psu_cortexa53_0/bspinclude/include -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\driver_vitis" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\cortex-a53" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\finsh" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\drivers\include" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\applications" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\components\libc\time" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\src" -I"D:\LearningMaterials\NAS\ZYNQ\AXU2CGB\StudyDoc\course_s2\00_zld\ps_rtt_hello\vitis\hello\src\rt-thread\libcpu\aarch64\common\gic" -c -fmessage-length=0 -MT"$@" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


