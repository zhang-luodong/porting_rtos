/*
 * Copyright (c) 2006-2020, RT-Thread Development Team
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs:
 * Date           Author         Notes
 * 2020-04-16     bigmagic       first version
 */

#ifndef BOARD_H__
#define BOARD_H__

#include <stdint.h>

#include "xparameters.h"
#include "xscugic.h"

#include "zldaxu2cgb.h"


#define ARM_GIC_NR_IRQS             (XSCUGIC_MAX_NUM_INTR_INPUTS)

extern unsigned char __bss_start;
extern unsigned char __bss_end;

#define RT_HW_HEAP_BEGIN    (void*)&__bss_end
#define RT_HW_HEAP_END      (void*)(RT_HW_HEAP_BEGIN + 4 * 1024 * 1024)

#define GIC_PL400_DISTRIBUTOR_PPTR  0x00F9010000    //���üĴ���
#define GIC_PL400_CONTROLLER_PPTR   0x00F9020000	//CPU�ӿڿ��ƼĴ���

void rt_hw_board_init(void);

#endif
